<?php
/**
 * @todo DOCUMENTAR ANTES DE SUBIR
 */
App::uses('FormHelper', 'View/Helper');

class BootstrapFormHelper extends FormHelper {

	public $helpers = ["Form", "Html"];

	private function textIcon($text, $icon) {
		return "<i class=\"fa ".$icon."\"></i> <span class=\"hidden-sm hidden-xs\">".$text."</span>";
	}

	public function create($model = null, $options = array()) {
		$options 	= Hash::merge($options, array());
		return parent::create($model, $options);
	}

	public function input($fieldName, $options = array(), $help = null) {

		$label 		= Hash::get($options, 'label');
		$labelText 	= null; 
		$after = null;
		$labelClass = 'control-label ';
		if(is_null($label)) {
			$labelText 	= Inflector::humanize($fieldName);
		} elseif(is_string($label)) {
			$labelText 	= $label;
		} elseif(is_array($label)) {
			$labelText 	= Hash::get($label, 'text');
			$labelClass	.= Hash::get($label, 'class');
		}

		if(!is_null($help)) {
		    $after = "<p class=\"help-block\">".$help."</p>";
		}

		$options 	= Hash::merge(array(
			'div'		=> array('class' => 'form-group'),
			'label'		=> array('class' => $labelClass, 'text' => $labelText),
			'class'		=> 'form-control',
			"after"		=> $after
		), $options);

		return parent::input($fieldName, $options);
	}

	public function textarea($fieldName, $options = array()) {
		$options 	= Hash::merge($options, array(
			'class' 	=> 'form-control',
			'rows'		=> 3
		));
		return parent::textarea($fieldName, $options);
	}

	/**
	 * @since 0.2
	 */	
	public function endWithoutButton() {
		return parent::end();
	}

	public function end($options = null, $secureAttributes = array()) {
		$text 	= 'Submit';
		if(is_string($options)) {
			$text 	= $options;
		} elseif(is_array($options)) {
			if($label = Hash::get($options, 'label')) {
				$text 	= $label;
			}
		}
		return 
			$this->button($text, array('class' => 'btn btn-success')).
			parent::end();
	}


	public function btnDelete($url = array(), $options = array(), $message = "Are you sure you want to delete?") {
		return $this->Form->postLink($this->textIcon("Delete", "fa-trash-o"), Hash::merge(array("action" => "delete"), $url), Hash::merge(array("class" => "btn btn-danger btn-sm", "escape" => false), $options), $message);
	}

	public function btn($text, $icon = null, $url = array(), $options = array(), $message = "Are you sure you want to delete?") {
		return $this->Form->postLink($this->textIcon($text, $icon), Hash::merge(array("action" => "delete"), $url), Hash::merge(array("class" => "btn btn-default btn-sm", "escape" => false), $options), $message);
	}

	public function btnActivation($active = true, $url = []) {
		if($active) {
			return $this->Form->postLink("Activado", Hash::merge(array("action" => "deactive"), $url), ['class' => 'btn btn-xs btn-success'], "Realmente desea desactivar este registro?");	
		}
		return $this->Form->postLink("Desactivado", Hash::merge(array("action" => "active"), $url), ['class' => 'btn btn-xs btn-warning'], "Realmente desea activar este registro?");
	}

}