<?php
/**
 * @todo DOCUMENTAR ANTES DE SUBIR
 */

App::uses("HtmlHelper", "View/Helper");

class BootstrapHtmlHelper extends HtmlHelper {

	public $helpers = array("Html", "Paginator");

	public function textIcon($text, $icon) {
		return "<i class=\"fa ".$icon."\"></i> <span class=\"hidden-sm hidden-xs\">".$text."</span>";
	}

	public function btn($text, $icon = null, $url, $options = [], $message = false) {
		return $this->Html->link($this->textIcon($text, $icon), $url, Hash::merge(["class" => "btn btn-primary btn-sm", "escape" => false], $options), $message);
	}

	public function btnList($url = array(), $options = array(), $message = false) {
		return $this->Html->link($this->textIcon("Listar", "fa-th-list"), Hash::merge(array("action" => "index"), $url), Hash::merge(array("class" => "btn btn-primary btn-sm", "escape" => false), $options), $message);
	}

	public function btnAdd($url = array(), $options = array(), $message = false) {
		return $this->Html->link($this->textIcon("Agregar", "fa-plus"), Hash::merge(array("action" => "add"), $url), Hash::merge(array("class" => "btn btn-success btn-sm", "escape" => false), $options), $message);
	}

	public function btnView($url = array(), $options = array(), $message = false) {
		return $this->Html->link($this->textIcon("Ver", "fa-eye"), Hash::merge(array("action" => "view"), $url), Hash::merge(array("class" => "btn btn-info btn-sm", "escape" => false), $options), $message);
	}

	public function btnIconView($url = array(), $options = array(), $message = false) {
		return $this->Html->link("<i class=\"fa fa-eye\"></i>", Hash::merge(array("action" => "view"), $url), Hash::merge(array("class" => "btn btn-info btn-sm", "escape" => false, "data-toggle" => "tooltip", 'title' => 'Ver Registro'), $options), $message);
	}

	public function btnEdit($url = array(), $options = array(), $message = false) {
		return $this->Html->link($this->textIcon("Editar", "fa-pencil"), Hash::merge(array("action" => "edit"), $url), Hash::merge(array("class" => "btn btn-warning btn-sm", "escape" => false), $options), $message);
	}

	public function btnDelete($url = array(), $options = array(), $message = "Are you sure you want to delete?") {
		return $this->Html->link($this->textIcon("Delete", "fa-trash-o"), Hash::merge(array("action" => "delete"), $url), Hash::merge(array("class" => "btn btn-danger btn-sm", "escape" => false), $options), $message);
	}

	public function btnActive($url = array(), $options = array(), $message = "Are you sure you want to active?") {
		return $this->Html->link(__("Desactivar"), Hash::merge(array("action" => "active"), $url), Hash::merge(array("class" => "btn btn-warning btn-xs"), $options), $message);
	}

	public function btnDeactive($url = array(), $options = array(), $message = "Are you sure you want to deactive?") {
		return $this->Html->link(__("Activar"), Hash::merge(array("action" => "deactive"), $url), Hash::merge(array("class" => "btn btn-success btn-xs"), $options), $message);
	}

	public function btnActivate($url = array(), $options = array(), $message = "Are you sure you want to active?") {
		return $this->Html->link(__("Activar"), Hash::merge(array("action" => "activate"), $url), Hash::merge(array("class" => "btn btn-success btn-xs"), $options), $message);
	}

	public function btnDeactivate($url = array(), $options = array(), $message = "Are you sure you want to deactive?") {
		return $this->Html->link(__("Desactivar"), Hash::merge(array("action" => "deactivate"), $url), Hash::merge(array("class" => "btn btn-warning btn-xs"), $options), $message);
	}

	public function btnLink($text = null, $url = array(), $options = array(), $message = false) {
		return $this->Html->link(__($text), $url, Hash::merge(array("class" => "btn btn-link btn-sm"), $options), $message);
	}

	public function linkModal($text = null, $url = array(), $options = array(), $message = false) {
		return $this->Html->link(__($text), $url, Hash::merge(array("data-toggle" => "modal", "data-target" => "#modal"), $options), $message);
	}

	public function listGroupItem($text, $url = array(), $options = array(), $message = false) {
		$active = "";
		if(Hash::contains($this->request->params, $url)) {
			$active = "active";
		}
		return $this->Html->link(__($text), Hash::merge($url, ["action" => "index"]), Hash::merge(array("class" => "list-group-item ".$active), $options), $message);		
	}

	public function simpleTable($data, $fields, $id, $paginator = true, $params = array()) {
		if(empty($data)) {
			return $this->jumbotronEmpty(Hash::get($id, 1), $params);
		}
		$result = "<table class=\"table\">";
		$result .= $this->simpleTHead($fields, $paginator);
		$result .= $this->simpleTBody($data, $fields, $id, $params);
		$result .= "</table>";
		return $result;
	}

	public function simpleTHead($fields, $paginator) {
		$result = "<thead>";
		$result .= "<tr>";
		foreach($fields as $field) {
			if($paginator) {
				if(is_array($field)) {
					$result .= "<th>".$this->Paginator->sort(Hash::get($field, "name"))."</th>";
				} else {
					$result .= "<th>".$this->Paginator->sort($field)."</th>";
				}
			} else {
				if(is_array($field)) {
					$result .= "<th>".Hash::get($field, "name")."</th>";
				} else {
					$result .= "<th>".$field."</th>";
				}
			}
		}
		$result .= "<th>Actions</th>";
		$result .= "</tr>";
		$result .= "</thead>";
		return $result;
	}

	public function simpleTBody($data, $fields, $id, $associatedId) {
		$result = "<tbody>";
		foreach($data as $value) {
			$result .= "<tr>";
			foreach($fields as $field) {

				if(is_array($field)) {
					$result .= "<td>".
						$this->linkModal(
							Hash::get($value, Hash::get($field, "name")), 
							array('controller' => Hash::get($field, "table"), 'action' => 'view', Hash::get($value, Hash::get($field, "id")))
						)
						."</td>";
				} else {

					if(strpos($field, ".active")) {
						$result .= "<td>";
						if(is_array($id)) {
							if(Hash::get($value, $field)) {
								$result .= $this->btnDeactive(array(Hash::get($value, Hash::get($id, 0)), 'controller' => Hash::get($id, 1)));
							} else {
								$result .= $this->btnActive(array(Hash::get($value, Hash::get($id, 0)), 'controller' => Hash::get($id, 1)));
							}
						} else {
							if(Hash::get($value, $field)) {
								$result .= $this->btnDeactive(Hash::get($value, $id));
							} else {
								$result .= $this->btnActive(Hash::get($value, $id));
							}
						}
						$result .= "</td>";
					} else {
						$result .= "<td>".Hash::get($value, $field)."</td>";
					}

				}

			}
			$result .= "<td width=\"200px\">";
			$result .= "<div class=\"btn-group btn-group-justified\">";
			if(is_array($id)) {
				$result .= $this->btnView(array(Hash::get($value, Hash::get($id, 0)), 'controller' => Hash::get($id, 1)))." ";
				$result .= $this->btnEdit(array(Hash::get($value, Hash::get($id, 0)), 'controller' => Hash::get($id, 1)))." ";
				$result .= $this->btnDelete(array(Hash::get($value, Hash::get($id, 0)), 'controller' => Hash::get($id, 1)));
			} else {
				$result .= $this->btnView(Hash::get($value, $id))." ";
				$result .= $this->btnEdit(Hash::get($value, $id))." ";
				$result .= $this->btnDelete(Hash::get($value, $id));
			}
			$result .= "</div>";
			$result .= "</td>";
			$result .= "</tr>";
		}
		$result .= "</tbody>";
		return $result;
	}

	public function jumbotronEmpty($controller, $params) {
		$result = "<div class=\"jumbotron\">";
  		$result .= "<h1>Ups...</h1>";
  		$result .= "<p>No hay registro cargados aun.</p>";
  		$result .= "<p>".$this->btnAdd(Hash::merge(array('controller' => $controller), $params))."</p>";
		$result .= "</div>";
		return $result;
	}

	public function simpleView($data, $fields, $id) {
		$result = "<dl>";
		foreach($fields as $field) {
			if(is_array($field)) {
				$result .= "<dt>".Hash::get($field, 1)."</dt>";
				$result .= "<dd>".
					$this->linkModal(Hash::get($data, Hash::get($field, "name")), array('controller' => Hash::get($field, "table"), 'action' => 'view', Hash::get($data, Hash::get($field, "id"))))
					."</dd>";
			} else {
				$result .= "<dt>".$field."</dt>";
				if(strpos($field, ".active")) {
					$result .= "<dd>";
					if(Hash::get($data, $field)) {
						$result .= $this->btnDeactive(Hash::get($data, $id));
					} else {
						$result .= $this->btnActive(Hash::get($data, $id));
					}
					$result .= "</dd>";
				} else {
					$result .= "<dd>".Hash::get($data, $field)."</dd>";
				}
			}
		}
		$result .= "</dt>";
		return $result;
	}

}