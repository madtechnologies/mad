<?php

/**
 * @todo DOCUMENTAR ANTES DE SUBIR
 */
App::uses('PaginatorHelper', 'View/Helper');

class BootstrapPaginatorHelper extends PaginatorHelper {

	public function numbers($options = array()) {
		$options 	= Hash::merge($options, array('tag' => 'li', 'separator' => false, 'currentTag' => 'a', 'currentClass' => 'active'));
		return parent::numbers($options);
	}

	public function prev($title = '<< Previous', $options = array(), $disabledTitle = null, $disabledOptions = array()) {
		$options 			= Hash::merge($options, array('tag' => 'li'));
		$disabledOptions 	= Hash::merge($options, array('disabledTag' => 'a', 'class' => 'disabled'));
		return parent::prev($title, $options, $disabledTitle, $disabledOptions);
	}

	public function next($title = 'Next >>', $options = array(), $disabledTitle = null, $disabledOptions = array()) {
		$options 			= Hash::merge($options, array('tag' => 'li'));
		$disabledOptions 	= Hash::merge($options, array('disabledTag' => 'a', 'class' => 'disabled'));
		return parent::next($title, $options, $disabledTitle, $disabledOptions);
	}

}