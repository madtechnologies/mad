<?php

/**
 * @todo Poner eventos con llamadas con security post, put
 */

/**
 * Uses: 
 * - Mad.Controller/CrudInterface
 * - CakeEvent/Event
 * - Mad.Controller/MadAppController
 */
App::uses("CrudInterface", "Mad.Controller/Interface");
App::uses('CakeEvent', 'Event');
App::uses("MadAppController", "Mad.Controller");

/**
 * Controller que implementa un Crud basico
 * Date 2015-10-14
 * @abstract
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 * @package Mad.Controller
 * @since 0.1
 * @version 0.1
 */
abstract class CrudController extends MadAppController implements CrudInterface {

	/**
	 * Alta de registro
	 * Dispara el evento Mad.Crud.afterSave
	 */
	public function admin_add() {
		if($this->request->is("post") && !empty($this->request->data)) {
			$ds = $this->{$this->modelClass}->getDataSource();
			$this->{$this->modelClass}->create();
			$data = $this->{$this->modelClass}->saveAll($this->request->data);
			if($data) {
				$event = new CakeEvent("Mad.Crud.afterAdd", $this, ['data' => $data]);
				$this->getEventManager()->dispatch($event);
				$this->Session->setFlash("Registro Guardado Exitosamente!");
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash("Registro no pudo ser Guardado!");
			}
		}
	}

	/**
	 * Borrado logico de registro
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
 	public function admin_delete($id = null) {
		if($this->request->is("post")) {
			if(is_null($id)) {
				throw new NotFoundException();
			}
			$data = $this->{$this->modelClass}->read(null, $id);
			if(empty($data)) {
				throw new NotFoundException();
			}
			$this->{$this->modelClass}->set("deleted", date("Y-m-d h:i:s"));
			if($this->{$this->modelClass}->save()) {
				$event = new CakeEvent("Mad.Crud.afterDelete", $this, ['data' => $data]);
				$this->getEventManager()->dispatch($event);
				$this->Session->setFlash("Registro Borrado Exitosamente!");
			} else {
				$this->Session->setFlash("Registro no pudo ser Borrado!");
			}
		} else {
			throw new NotFoundException();
		}
		$this->redirect(['action' => 'index']);
	}

	/**
	 * Modificación de registros con transaccion de DB.
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
	public function admin_edit($id = null) {
		if(is_null($id)) {
			throw new NotFoundException();
		}
		$data = $this->{$this->modelClass}->read(null, $id);
		if(empty($data)) {
			throw new NotFoundException();
		}
		if($this->request->is("put") && !empty($this->request->data)) {
			$this->request->data = Hash::merge([$this->modelClass => [$this->{$this->modelClass}->primaryKey => $id]], $this->request->data);
			if($this->{$this->modelClass}->saveAll($this->request->data)) {
				$event = new CakeEvent("Mad.Crud.afterEdit", $this, ['data' => $data]);
				$this->getEventManager()->dispatch($event);
				$this->Session->setFlash("Registro Modificado Exitosamente!");
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash("Registro no pudo ser Modificado!");
			}
		}
		$this->request->data = $data;
	}

	/**
	 * Listado de registros con paginador
	 */
	public function admin_index() {
		$this->Paginator->settings['conditions'] = Hash::merge(
			[$this->modelClass.'.deleted' => null], 
			Hash::extract($this->Paginator->settings, "conditions")
		);
		$data = $this->Paginator->paginate();
		$this->set("data", $data);
	}

	/**
	 * Vista de registro 
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
	public function admin_view($id = null) {
		if(is_null($id)) {
			throw new NotFoundException();
		}
		$data = $this->{$this->modelClass}->read(null, $id);
		if(empty($data)) {
			throw new NotFoundException();
		}
		$this->set("data", $data);
	}	

}