<?php

/**
 * Interface de Controller para Actions basicas (CRUD)
 * Date: 2015-10-14
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 * @package Mad.Controller/Interface
 * @since 0.1
 * @version 0.1
 */

interface CrudInterface {

	/**
	 * Listado de registros
	 */
	public function admin_index();

	/**
	 * Alta de registro
	 */
	public function admin_add();

	/**
	 * Vista de un registro
	 * @var $id del Modelo
	 */
	public function admin_view($id = null);

	/**
	 * Modificacion de un registro
	 * @var $id del Modelo
	 */
	public function admin_edit($id = null);

	/**
	 * Borrado logico del registro
	 * @var $id del Modelo
	 */
	public function admin_delete($id = null);

}