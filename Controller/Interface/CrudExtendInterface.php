<?php

/**
 * Uses: 
 * - Mad.Controller/Interface/CrudInterface
 */
App::uses("CrudInterface", "Mad.Controller/Interface");

/**
 * Interface de Controller para Actions de manipulacion de registros mas alla del CRUD
 * Date: 2015-10-14
 * @author Hernan Iglesias <higlesias@msal.gov.ar>
 * @version 0.1
 * @since 0.1
 * @package Mad.Controller
 */
interface CrudExtendInterface extends CrudInterface {

	/**
	 * Activacion de un registro
	 * @var $id del Modelo
	 */
 	public function admin_active($id = null);

	/**
	 * Desactivacion de un registro
	 * @var $id del Modelo
	 */
 	public function admin_deactive($id = null);

	/**
	 * Listado de registros borrados logicamente
	 * @var $id del Modelo
	 */
 	public function admin_trash();

	/**
	 * Restauracion de un registro borrado logicamente
	 * @var $id del Modelo
	 */
 	public function admin_restore($id = null);

	/**
	 * Borrado fisico de un registro
	 * @var $id del Modelo
	 */
 	public function admin_destroy($id = null);

}