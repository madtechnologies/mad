<?php

/**
 * Uses: 
 * - Mad.Controller/CrudController
 * - Mad.Controller/Interface/CrudExtendInterface
 */
App::uses("CrudController", "Mad.Controller");
App::uses("CrudExtendInterface", "Mad.Controller/Interface");

/**
 * Controller que implementa un Crud extendido
 * Date 2015-10-14
 * @abstract
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 * @package Mad.Controller
 * @since 0.1
 * @version 0.1
 */
abstract class CrudExtendController extends CrudController implements CrudExtendInterface {

	/**
	 * Activacion de un registro
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
 	public function admin_active($id = null) {
		if($this->request->is("post")) {
			if(is_null($id)) {
				throw new NotFoundException();
			}
			$data = $this->{$this->modelClass}->read(null, $id);
			if(empty($data)) {
				throw new NotFoundException();
			}
			$this->{$this->modelClass}->set("active", true);
			if($this->{$this->modelClass}->save()) {
				$this->Session->setFlash("Registro Activado Exitosamente!");
			} else {
				$this->Session->setFlash("Registro no pudo ser Activado!");
			}
		} else {
			throw new NotFoundException();
		}
		$this->redirect(['action' => 'index']);
	}


	/**
	 * Activacion de un registro
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
 	public function admin_deactive($id = null) {
		if($this->request->is("post")) {
			if(is_null($id)) {
				throw new NotFoundException();
			}
			$data = $this->{$this->modelClass}->read(null, $id);
			if(empty($data)) {
				throw new NotFoundException();
			}
			$this->{$this->modelClass}->set("active", false);
			if($this->{$this->modelClass}->save()) {
				$this->Session->setFlash("Registro Desactivado Exitosamente!");
			} else {
				$this->Session->setFlash("Registro no pudo ser Desactivado!");
			}
		} else {
			throw new NotFoundException();
		}
		$this->redirect(['action' => 'index']);
	}

	/**
	 * Borrado fisico de registro
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
 	public function admin_destroy($id = null) {
		if($this->request->is("post")) {
			if(is_null($id)) {
				throw new NotFoundException();
			}
			$data = $this->{$this->modelClass}->read(null, $id);
			if(empty($data)) {
				throw new NotFoundException();
			}
			if($this->{$this->modelClass}->delete()) {
				$this->Session->setFlash("Registro Eliminado Exitosamente!");
			} else {
				$this->Session->setFlash("Registro no pudo ser Eliminado!");
			}
		} else {
			throw new NotFoundException();
		}
		$this->redirect(['action' => 'index']);
	}	

	/**
	 * Restauracion de un registro borrado logicamente
	 * @throws NotFoundException
	 * @var $id del Modelo
	 */
 	public function admin_restore($id = null) {
		if($this->request->is("post")) {
			if(is_null($id)) {
				throw new NotFoundException();
			}
			$data = $this->{$this->modelClass}->read(null, $id);
			if(empty($data)) {
				throw new NotFoundException();
			}
			$this->{$this->modelClass}->set("deleted", null);
			if($this->{$this->modelClass}->save()) {
				$this->Session->setFlash("Registro Restaurado Exitosamente!");
			} else {
				$this->Session->setFlash("Registro no pudo ser Restaurado!");
			}
		} else {
			throw new NotFoundException();
		}
		$this->redirect(['action' => 'index']);
	}


	/**
	 * Listado de registros borrados logicamente con paginador
	 */
	public function admin_trash() {
		$this->Paginator->settings['conditions'] = Hash::merge(
			['NOT' => [$this->modelClass.'.deleted' => null]], 
			Hash::extract($this->Paginator->settings, "conditions")
		);
		$data = $this->Paginator->paginate();
		$this->set("data", $data);
	}

}