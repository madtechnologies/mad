<?php

/**
 *
 */

/**
 *
 */
App::uses("BasicAuthenticate", "Controller/Component/Auth");

/**
 *
 */
class StaticAuthenticate extends BasicAuthenticate {

	/**
	 *
	 */
	public function getUser(CakeRequest $request) {
	    $username = env("PHP_AUTH_USER");
	    $password = env("PHP_AUTH_PW");

	    if (empty($username) || empty($password)) {
	        return false;
	    }

	    if($username === Configure::read("Users.admin.username") && 
	    	$password === Configure::read("Users.admin.password")){
	    	return ['User' => ['username' => Configure::read("Users.admin.username")]];
	    }

		return false;
	}

}