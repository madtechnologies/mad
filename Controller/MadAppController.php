<?php

/**
 * @todo chequear de crear un user login simple con el SimpleAuth de mad.
 */

/**
 * Uses: 
 * - Controller/Controller
 */
App::uses('Controller', 'Controller');

/**
 * Controller inicial de la App donde confifigura todo los components, helpers y demas
 * Date 2015-10-12
 * @version 0.1
 * @since 0.1
 * @package Mad.Controller
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class MadAppController extends Controller {

	/**
	 *
	 */
 	public $components = [
		'Acl',
		'Auth' => [
			'loginAction' => '/login',
			'authenticate' => [
				'Form' => [
					'userModel' => 'Users.User',
					'passwordHasher' => [
						'className' => 'Simple',
						'hashType' => 'sha256'
					],
					'scope' => ['User.active' => true, 'User.deleted' => null]
				]
			],
			'authorize' => ['Actions' => ['actionPath' => 'controllers']]
		],
		'Paginator',
		'RequestHandler',
		'Security' => ['callback' => 'blackhole'],
		'Session',
		'DebugKit.Toolbar',
	];

	/**
	 *
	 */
 	public $helpers = [
		'Form' => ['className' => 'Mad.BootstrapForm'],
		'Html' => ['className' => 'Mad.BootstrapHtml'],
		'Paginator' => ['className' => 'Mad.BootstrapPaginator'],
		'Session'
	];

	/**
	 * @throws NotFoundException
	 */
	public function blackhole($type = null) {
		throw new NotFoundException();
	}

	/**
	 * @todo Bugfix Cargo el User plugin para que lo lea
	 * Verifico si el prefix del request es admin, para setear los themes
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->loadModel("Users.User");
		$this->User;
		if(is_null(Hash::get($this->request->params, "prefix"))) {
			$this->theme = Configure::read("Mad.ThemeWeb");
		} else {
			$this->theme = Configure::read("Mad.ThemeAdmin");
		}
	}

}
