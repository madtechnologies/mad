<?php

/**
 * Clase para ayudas con strings.
 * Date 2015-10-12
 * @version 0.1
 * @since 0.1
 * @package Mad.Utility
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class MadString {

	/**
	 * @see http://stackoverflow.com/questions/4356289/php-random-string-generator
	 * Copiado de funcion para generar un string aleatoreo para el password
	 * @param $length cantidad de caracteres
	 * @return String random
	 */
	public static function stringGenerator($length = 8) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/**
	 * @see http://stackoverflow.com/questions/1586389/how-to-check-for-a-duplicate-email-address-in-php-considering-gmail-user-name
	 * Elimina los . y + en los emails
	 * @param string $email Correo electronico
	 * @return string
	 */
	public static function normalizeEmails($email) {
		$email_parts = explode('@', $email);
		$before_plus = strstr($email_parts[0], '+', TRUE);
		$before_at = $before_plus ? $before_plus : email_parts[0];
		$before_at = str_replace('.', '', $before_at);
		return $before_at.'@'.$email_parts[1];
	}

}