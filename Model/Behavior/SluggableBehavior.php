<?php

/**
 *
 */

/**
 *
 */

/**
 *
 */
class SluggableBehavior extends ModelBehavior {

	/**
	 *
	 */
	private $defaults = [
		"field" => "title",
		"unique" => true
	];

	/**
	 *
	 */
	public function setup(Model $model, $settings = array()) {
		$this->settings[$model->alias] = Hash::merge($this->defaults, $settings);
		$this->model = $model;
	}

	/**
	 *
	 */
	public function afterSave(Model $model, $created, $options = []) {
		parent::afterSave($model, $created, $options);
		if($created) {
			$model->id;
			$field = Hash::get($this->settings, $model->alias.".field");
			$model->set("slug", $this->createSlug(Hash::get($model->data, $model->alias.".".$field)));
			$this->createSlug(Hash::get($model->data, $model->alias.".".$field));
			return $model->save();
		}
	}

	/**
	 *
	 */
	private function createSlug($slug, $i = null) {
		$slugUri = $this->slugUri($slug." ".$i);
		if($this->settings[$this->model->name]["unique"]) {
			$exist = $this->model->findBySlug($slugUri);
			if(empty($exist)) {
				return $slugUri;
			} else {
				if(is_null($i)) {
					$i = 1;
				} else {
					$i++;
				}
				return $this->createSlug($slug, $i);
			}
		} else {
			return $slugUri;
		}
	}

	/**
	 * @see http://www.hashbangcode.com/blog/creating-uri-slug-php
	 */
	private function slugUri($string) {
		$slug = trim($string);
		$slug = preg_replace("/[ñÑ]/u", "n", $slug);
		$slug = preg_replace("/[^a-zA-Z0-9 ]/","",$slug );
		$slug = str_replace(" ","-", $slug);
		$slug = strtolower($slug);
		return $slug;		
	}

}