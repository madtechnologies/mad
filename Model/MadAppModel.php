<?php

/**
 *
 */
App::uses('Model', 'Model');

/**
 *
 */
class MadAppModel extends Model {

	/**
	 *
	 */
	public $recursive = -1;

	/**
	 *
	 */
	public $actsAs = [
		'Containable'
	];

}